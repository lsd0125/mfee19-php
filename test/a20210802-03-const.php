<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div>
<?php

define('MY_CONST', 234); // 自訂常數


echo MY_CONST;
echo '<br>';

echo true; // True, TRUE, 不分大小寫都可以
echo '<br>';
echo false;

?>
</div>
</body>
</html>
<h3>
<?php

for($i=1; $i<10; $i++){
    $br[] = $i*$i;
}
array_push($br, 100, 101);

echo json_encode($br); // 轉換成 JSON 字串

echo '<br>------------<br>';

$ar = array(
    'name' => '李小明/帥',
    'age' => 23,
    'gender' => '男生',
);
echo json_encode($ar);
echo '<br>------------<br>';
echo json_encode($ar, JSON_UNESCAPED_UNICODE); // 不跳脫中文字
echo '<br>------------<br>';
echo json_encode($ar, JSON_UNESCAPED_SLASHES); // 不跳脫 slash
echo '<br>------------<br>';
echo json_encode($ar, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE); // 不跳脫中文字, 同時不跳脫 slash
echo '<br>------------<br>';

?>
</h3>
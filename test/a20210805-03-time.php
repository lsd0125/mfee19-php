<h2>
    <?php
    // 解析時間格式
    // https://stackoverflow.com/questions/2767324/how-to-parse-a-date-string-in-php


    // 設定時區
    // date_default_timezone_set('Asia/Taipei');

    echo time(). '<br>';
    echo date("Y-m-d H:i:s"). '<br>';
    echo date("D  N  w"). '<br>';
    echo date("Y-m-d H:i:s", time()+40*24*60*60). '<br>';

    $t = strtotime('2021/09/06'); // timestamp
    echo date("Y-m-d H:i:s", $t). '<br>';
    ?>
</h2>
